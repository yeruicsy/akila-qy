import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false

App.mpType = 'app'

//装配Vuex
import store from "./lib/store";
Vue.prototype.$store = store;

//全局自定义组件
import AkilaList from "./components/akila-list.vue"
import AkilaTimeLine from "./components/akila-time-line.vue"
import AkilaAlbum from "./components/akila-album.vue"
import AkilaPageLoad from "./components/akila-page-load.vue"
import AkilaLoading from "./components/akila-loading.vue"
import UniPopup from "./components/uni-popup/uni-popup.vue"
import AkilaButton from "./components/akila-button.vue"
Vue.component("akila-list", AkilaList);
Vue.component("akila-time-line", AkilaTimeLine);
Vue.component("akila-album", AkilaAlbum);
Vue.component("akila-page-load", AkilaPageLoad);
Vue.component("akila-loading", AkilaLoading);
Vue.component("uni-popup", UniPopup);
Vue.component("akila-button", AkilaButton);

//安装框架
import UtilVueCore from "./lib/index";
Vue.use(UtilVueCore, {
	ui: "uni-app",
	serverConfig: {
		// systemServer: "http://192.168.100.68"
		// systemServer: "http://192.168.100.25"
		
		systemServer: "https://test.akila3d.com/api"
		// systemServer: process.env.NODE_ENV == 'development' ? "http://192.168.100.68" : "http://58.246.64.11:20136",
		// systemServer: "https://akilaapi.adenservices.com"
		// systemServer: "https://platform.akila3d.com/api"
		// systemServer: "https://demo.akila3d.com/api"
	}
});

//国际化
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)
const i18n = new VueI18n({
	locale: 'zh', // 定义默认语言为中文 
	messages: {
		'zh': require('@/assets/languages/zh.json'),
		'en': require('@/assets/languages/en.json')
	}
});

const app = new Vue({
	...App,
	i18n
})
app.$mount()
