let components = {}
let requireComponents = require.context('./', true, /\.js$/)

requireComponents.keys().forEach(path => {
	let name = /\/(.*?).js/.exec(path)[1];
	let arr = name.split('/');
	name = arr[arr.length - 1];
	let conponent = requireComponents(path);
	if (name === "index" || name === "store_init")
		return;
	components[name] = conponent.default || conponent
})

export default components
