import {
	util
} from "./../../../util/index";

const state = {
	projectDatas: undefined, //我的项目数据
	selectedProject: undefined, //当前选择的项目
};

const getters = {

};

const mutations = {
	/** 设置我的项目缓存 */
	setProjectDatas(state, config) {
		state.projectDatas = config.datas;
		if (!state.projectDatas || state.projectDatas.length === 0)
			return;
		if (state.selectedProject && state.selectedProject.code) {
			if (state.projectDatas.filter(t => t.code === state.selectedProject.code).length === 0) {
				mutations.setSelectedProject(state, state.projectDatas[0])
			} else {
				mutations.setSelectedProject(state, state.projectDatas.filter(t => t.code === state.selectedProject.code)[0])
			}
		} else {
			mutations.setSelectedProject(state, state.projectDatas[0])
		}
	},
	/** 设置项目 */
	setSelectedProject(state, data) {
		state.selectedProject = data;
		util.globalHeader.add('projectCode', state.selectedProject.code);
	},
	/** 设置项目 */
	setSelectedProjectByCode(state, code) {
		if (state.projectDatas.filter(t => t.code === state.selectedProject.code).length !== 0) {
			mutations.setSelectedProject(state, state.projectDatas.filter(t => t.code === code)[0])
		}
	},
};

const actions = {
	/** 获取项目信息 */
	async getProjectInfoAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		var url = `${util.url.project.getProjectInfo}?id=` + request.data.id;
		return await new Promise((resolve, reject) => {
			util.webApi.post(url, request, resolve);
		});
	},
	/** 获取所有项目 */
	async getAllProjectInfoAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		var result = await new Promise((resolve, reject) => {
			util.webApi.get(util.url.project.getAllProjectInfo, request, resolve);
		});
		if (result)
			commit('setProjectDatas', {
				defaultCode: rootState.user.projectCode,
				datas: result
			});
		return result;
	},

};


export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
