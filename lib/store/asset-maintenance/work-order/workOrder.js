import {
	util
} from "./../../../util/index";

const state = {

};

const getters = {

};

const mutations = {

};

const actions = {
	/** 工单的分页查询 */
	async workAreaOrderPagerQueryAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.get(util.url.assetMaintenance.workAreaOrderPagerQuery, request, resolve);
		});
	},
	/** 添加公区工单 */
	async addWorkAreaOrderAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.post(util.url.assetMaintenance.addWorkAreaOrder, request, resolve);
		});
	},
	/** 获取工单 */
	async getWorAreakOrderListDetailAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.get(util.url.assetMaintenance.getWorAreakOrderListDetail, request, resolve);
		});
	},
	/** 上传文件 */
	async batchUploadAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.post(util.url.assetMaintenance.batchUpload, request, resolve);
		});
	},
	/** 获取图片 */
	async getPictureUrlAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.get(util.url.assetMaintenance.getPictureUrl, request, resolve);
		});
	},
	/** serverId获取图片预览 */
	async getPictureUrlByServerId({
		dispatch,
		commit,
		state,
		rootState,
		rootGetters
	}){
		return await new Promise((resolve, reject) => {
			util.webApi.pot(util.url.assetMaintenance.getImageUrl, request, resolve);
		})
	},
	/** 工单日志 */
	async getWorkOrderLogsAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.get(util.url.assetMaintenance.workOrderLogs, request, resolve);
		});
	},
	/** 获取位置分页  */
	/** 获取位置不分页  */
	async getPositionList({
		dispatch,
		commit,
		state,
		rootState,
		rootGetters
	}, request) {
		return await new Promise((resolve, reject) => {
			util.webApi.get(util.url.assetMaintenance.getPositionList, request, resolve);
		})
	},
	/** 微信工单发送评价 */
	async wechatWorkOrderEvaluateAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.post(util.url.assetMaintenance.wechatWorkOrderEvaluate, request, resolve);
		});
	},
	/** 发送工单签名 */
	async wechatUploadWorkOrderSignatureAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.post(util.url.assetMaintenance.wechatUploadWorkOrderSignature, request, resolve);
		});
	},
	/** 获取工单签名 */
	async getWorkOrderSignatureAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		id
	) {
		return await new Promise((resolve, reject) => {
			//下载签名
			uni.downloadFile({
				url: `${util.url.assetMaintenance.getWorkOrderSignature}?id=${id}`, //仅为示例，并非真实的资源
				header: util.globalHeader.toObject(),
				success: (res) => {
					if (res.statusCode === 200) {
						console.log('下载成功');
						resolve(res.tempFilePath);
						return
					}
					resolve(null);
				},
				fail: () => {
					resolve(null);
				}
			})
		});
	},
	/** 获取项目工单类型 */
	async getWorkOrderTypeList({
		
	}, request) {
		return await new  Promise((resolve, reject) => {
			util.webApi.get(util.url.assetMaintenance.getWorkOrderType, request, resolve)
		})
	}

};


export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
