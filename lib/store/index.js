import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
import createPersistedState from 'vuex-persistedstate'
import components from './store_init.js'

var modules = {
	...components
};

const store = new Vuex.Store({
	modules: modules,
	plugins: [createPersistedState({
		key: 'app_config_data', // 状态保存到本地的 key   
		paths: [
			'user.userInfo', 'user.oldToken', 'user.token', 'user.projectCode', 'project.projectDatas', 'project.selectedProject', 'user.qyUserInfo'
		], // 要持久化的状态，在state里面取，如果有嵌套，可以  a.b.c   
		storage: { // 存储方式定义
			//uni使用
			getItem: (key) => uni.getStorageSync(key), // 获取  
			setItem: (key, value) => uni.setStorageSync(key, value), // 存储  
			removeItem: (key) => uni.removeStorageSync(key), // 删除 
		}
	})],
	strict: true
});

export default store;
