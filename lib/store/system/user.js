import {
	util
} from "./../../util/index";

const state = {
	wxConfig: {
		appId: "ww124a6091e76ba1c3"//企业微信的Appid 测试团段
		// appId: "ww690976a0da383f77",//企业微信的Appid aden测试团队
		// appId: "wxab9ee503e6ae26f8",//企业微信的Appid 联影
	},
	token: null,//身份
	oldToken: null,//身份
	projectCode: null,//租户编号
	userInfo: null,//用户信息缓存
	wechatUserInfoTemp: null,//微信返回的用户信息临时全局变量
	loginRetryCountLimit: 4,//登录尝试性重复次数
	isSubscribe: true,//用户是否关注公众号
	qyUserInfo: null, //企业微信用户信息
	accessToken: null
};

const getters = {
	/** 是否超过限制 **/
	isLoginRetryMoreThanLimit(state) {
		var loginRetryCount = uni.getStorageSync("loginRetryCount") || "0";
		loginRetryCount = parseInt(loginRetryCount);
		return state.loginRetryCountLimit < loginRetryCount;
	}
};

const mutations = {
	/** 设置登录成功 */
	setLogin(state, config) {
		state.oldToken = config.oldToken;
		state.token = `${config.tokenHead}${config.token}`;
		if (config.projectInfo.length > 0)
			state.projectCode = 'westwisdomvalley';
		state.userInfo = config;
		util.globalHeader.add('Token', config.oldToken);
		if (config.projectInfo.length > 0)
			util.globalHeader.add('projectCode', 'westwisdomvalley');
		util.globalHeader.add('sessionKey', `${config.tokenHead}${config.token}`);
	},
	/** 企业微信用户信息 */
	setQyUserInfo(state, config) {
		state.qyUserInfo = config
	},
	/** 设置项目编号 */
	setProjectCode(state, projectCode) {
		state.projectCode = projectCode;
		util.globalHeader.add('projectCode', projectCode);
	},
	setAaccessToken(state, accessToken) {
		state.accessToken = accessToken;
	},
	/** 登出删除缓存token */
	loginOut(state) {
		state.oldToken = null;
		state.token = null;
		state.projectCode = null;
		state.userInfo = null;
		util.globalHeader.remove('Token');
		util.globalHeader.remove('projectCode');
		util.globalHeader.remove('sessionKey');
	},
	/** 临时微信信息存储 */
	setWechatUserInfoTemp(state, data) {
		state.wechatUserInfoTemp = data
	},
	/** 重置Token */
	resetToken(state) {
		util.globalHeader.add('Token', state.oldToken);
		util.globalHeader.add('projectCode', state.projectCode);
		util.globalHeader.add('sessionKey', state.token);
	},
	/* 重设登录重试次数 */
	resetLoginRetryCount(state) {
		uni.setStorageSync("loginRetryCount", 0)
	},
	/* 登录失败记忆次数 */
	loginErrorRetry(state) {
		state.loginRetryCount += 1;
		var loginRetryCount = uni.getStorageSync("loginRetryCount") || "0";
		loginRetryCount = parseInt(loginRetryCount);
		loginRetryCount += 1;
		uni.setStorageSync("loginRetryCount", loginRetryCount)
	},
	/** 设置是否订阅 */
	setIsSubscribe(state, data) {
		state.isSubscribe = data;
	}
};

const actions = {
	/** 公众号登录 */
	async mpCodeLoginAsync({
			dispatch,
			commit,
			getters,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		request.error = (e) => {
			if (e.code === 10001) {
				commit('setIsSubscribe', false);
			}
		}
		var result = await new Promise((resolve, reject) => {
			util.webApi.post(util.url.system.mpCodeLogin, request, resolve);
		});
		if (result && result.oldToken) {
			commit('setLogin', result);
			dispatch('loginSuccessAsync');
		}
		if (result.wechatUserInfo)
			commit('setWechatUserInfoTemp', result.wechatUserInfo);
		return result;
	},
	/** 企业code获取企业员工信息 */
	async qyCodeLoginAsync ({
		dispatch,
		commit,
		getters,
		state,
		rootState,
		rootGetters
	}, request) {
		var result = await new Promise((resolve, reject) => {
			util.webApi.get(util.url.system.qyCodeLogin, request, resolve);
		});
		if (result.errcode != 0) {
			util.message.toast('获取员工信息失败')
			return;
		}
		if (result) {
			commit('setQyUserInfo', result)
		}
		return result;
	},
	/** 发送注册短信 */
	async getSMSCodeAsync({
			dispatch,
			commit,
			getters,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		var result = await new Promise((resolve, reject) => {
			util.webApi.post(util.url.system.getSMSCode, request, resolve);
		});
		return result;
	},
	/** 公众号注册 */
	async mpRegisterAsync({
			dispatch,
			commit,
			getters,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		var result = await new Promise((resolve, reject) => {
			util.webApi.post(util.url.system.mpRegister, request, resolve);
		});
		if (!result)
			return
		if (!result.projectInfo || result.projectInfo.length === 0) {
			console.log("没有返回项目信息")
			util.message.toast('没有返回项目信息')
			return;
		}
		if (result.oldToken)
			commit('setLogin', result);
		if (result.wechatUserInfo)
			commit('setWechatUserInfoTemp', result.wechatUserInfo);
		dispatch("project/getAllProjectInfoAsync", null, {
			root: true
		})
		return result;
	},
	/** 公众号注册 */
	async addProjectAsync({
			dispatch,
			commit,
			getters,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		var result = await new Promise((resolve, reject) => {
			util.webApi.post(util.url.system.mpRegister, request, resolve);
		});
		await dispatch("project/getAllProjectInfoAsync", null, {
			root: true
		})
		return result;
	},
	/** 刷新token */
	async refreshTokenAsync({
			dispatch,
			commit,
			getters,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		if (!state.token)
			return;
		commit('resetToken');
		request = request || {};
		request.errorMessage = false;
		var result = await new Promise((resolve, reject) => {
			util.webApi.post(util.url.system.token, request, resolve);
		});
		if (!result) {
			commit('loginOut');
			uni.navigateTo({
				url: "/pages/user/login.vue"
			})
		}
		dispatch('loginSuccessAsync');
		return result;
	},
	/** 登录成功 */
	async loginSuccessAsync({
			dispatch,
			commit,
			getters,
			state,
			rootState,
			rootGetters
		},
		request) {
		commit('resetLoginRetryCount');
		await dispatch("project/getAllProjectInfoAsync", null, {
			root: true
		})
	},
	/** 重置登录状态 */
	async resetTokenAsync({
		dispatch,
		commit,
		getters,
		state,
		rootState,
		rootGetters
	}) {
		if (!rootState.project.selectedProject || !rootState.project.selectedProject.code)
			return;
		if (rootState.project.selectedProject.code !== state.projectCode)
			commit('setProjectCode', rootState.project.selectedProject.code);
		commit('resetToken')
	},
	/** 重置登录状态 */
	async setProjectCodeForceAsync({
		dispatch,
		commit,
		getters,
		state,
		rootState,
		rootGetters
	}, code) {
		debugger
		commit('setProjectCode', code);
		commit('project/setSelectedProjectByCode', code, {
			root: true
		})
	},
	/** 退出系统清空 */
	async loginOutAsync({
		dispatch,
		commit,
		getters,
		state,
		rootState,
		rootGetters
	}) {
		commit('loginOut');
		commit('project/loginOut', null, {
			root: true
		});
	},
	/** 登陆默认账号 */
	async loginAccount({
		dispatch,
		commit,
		getters,
		state,
		rootState,
		rootGetters
	}, request) {
		var result = await new Promise((resolve, reject) => {
			util.webApi.post(util.url.system.loginAccount, request, resolve);
		});
		if (!result)
			return
		if (result && result.oldToken) {
			commit('setLogin', result);
		}
		return result;
	}
};


export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
