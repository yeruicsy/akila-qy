import {
	util
} from "./../../util/index";
var wx = require('weixin-js-sdk')

const state = {
	iosWxSignUrl: "", // 兼容IOS history模式  url不变更的问题  需要程序打开时对第一个url进行记录
	templates: {
		allNotofy: 'BcYLXoLBaLEnk6sdB2PxieERV5oO2dlVs2CxaWYBKKA', // 全局通知模板
		workOrderProgressNotify: 'aNiOIJqT5km8Zhe3D2a2JeQlsZwzksN690IX5dQsa_c', //工单通知模板
	},
	platformType: { //平台枚举
		mp: '1', //公众号
		miniProgram: '2', //小程序
	},
	wxReady: undefined, //微信sdk 是否准备就绪 目前在旧版本2019年微信依然存在兼容性问题  目前未使用 未来理应使用此变量不进行sdk 重复注册
};

const getters = {
	getWechatSignUrl: (state) => state.wxSignUrl
};

const mutations = {
	setWechatSignUrl(state, wxSignUrl) {
		state.iosWxSignUrl = wxSignUrl;
	},
	setWxReady(state, data) {
		if (state.wxReady !== undefined)
			return;
		state.wxReady = data;
	}
};

const actions = {
	/** 注册企业微信SDK */
	async registSdkAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		var result = false;
		for (var i = 0; i < 2; i++) {
			result = await dispatch('registSdkHandleAsync', request)
			if (result === true)
				return result;
		}
		return result;
	},
	/** 注册企业微信SDK */
	async registSdkHandleAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		request = request || {};
		//签名开始
		var result = await new Promise((resolve, reject) => {
			request.data = {
				url: window.location.href.split('#')[0]
			}
			console.log(request.data.url)
			request.mask = true;
			util.webApi.post(util.url.system.jsSdkConfig, request, resolve);
		});
		console.log(result)
		if (!result)
			return;
		//使用签名进行注册
		result = await new Promise((resolve, reject) => {
			wx.config({
				beta: true,// 必须这么写，否则wx.invoke调用形式的jsapi会有问题
				debug: false , // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				appId: result.jsAPISignature.appId, // 必填，公众号的唯一标识
				timestamp: result.jsAPISignature.timestamp, // 必填，生成签名的时间戳
				nonceStr: result.jsAPISignature.nonce, // 必填，生成签名的随机串
				signature: result.jsAPISignature.signature, // 必填，签名
				jsApiList: [
					"updateAppMessageShareData",
					"updateTimelineShareData",
					"onMenuShareWeibo",
					"onMenuShareQZone",
					"startRecord",
					"stopRecord",
					"onVoiceRecordEnd",
					"playVoice",
					"pauseVoice",
					"stopVoice",
					"onVoicePlayEnd",
					"uploadVoice",
					"downloadImage",
					"downloadVoice",
					"chooseImage",
					"previewImage",
					"uploadImage",
					"downloadImage",
					"translateVoice",
					"getNetworkType",
					"openLocation",
					"getLocation",
					"getLocalImgData",
					"hideOptionMenu",
					"showOptionMenu",
					"hideMenuItems",
					"showMenuItems",
					"hideAllNonBaseMenuItem",
					"showAllNonBaseMenuItem",
					"closeWindow",
					"scanQRCode",
					"openProductSpecificView",
					"addCard",
					"chooseCard",
					"openCard"
				], // 必填，需要使用的JS接口列表
			});
			wx.ready(function(res) {
				wx.error(function(res) {
					commit('setWxReady', false);
					resolve(false)
				});
				setTimeout(() => {
					commit('setWxReady', true);
					resolve(true);
				}, 100)
				return
			});
		});
		return result;
	},
	/** 查询模板是否同意 */
	async queryTemplateAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.get(util.url.system.queryTemplate, request, resolve);
		});
	},
	/** 创建模板 */
	async createTemplateAsync({
			dispatch,
			commit,
			state,
			rootState,
			rootGetters
		},
		request
	) {
		return await new Promise((resolve, reject) => {
			util.webApi.post(util.url.system.createTemplate, request, resolve);
		});
	}
};


export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
