import Vue from "vue";

import {
	Helper
} from "./common/helper";
import url from "./base/url";
import {
	Global
} from "./base/global";
import {
	Message
} from "./message/message";
import {
	GlobalHeader
} from "./http/globalHeader";
import {
	HttpClient
} from "./http/httpClient";
import {
	WebApi
} from "./http/webapi";
import {
	Loading
} from "./loading/loading";
import {
	Storage
} from "./../util/storage/storage";
import {
	ActionSheet
} from "./../util/actions/actionSheet";
import {
	QueryModel
} from "./../util/common/pager";
import {
	RequestModel
} from "./../util/common/requestModel";

var staticUtil = {
	helper: new Helper(),
	message: new Message(),
	global: new Global(),
	globalHeader: new GlobalHeader(),
	loading: new Loading(),
	url: url,
	webApi: new WebApi(),
	storage: new Storage(),
	actionSheet: new ActionSheet(),
	httpClient: new HttpClient(),
	createPagerQuery: () => {
		return new QueryModel();
	},
	createRequest: (_data, _loading, _errorMessage, _btn) => {
		return new RequestModel(_data, _loading, _errorMessage, _btn);
	},
};

export {
	staticUtil as util
};
