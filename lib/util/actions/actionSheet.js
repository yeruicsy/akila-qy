import {
	util
} from "./../index";
export class ActionSheet {

	/**
	 * 操作菜单
	 * @param {*} btns [{ text:'按钮文字1' , callback:func1 },{ text:'按钮文字2' , callback:func2 }]
	 */
	open(btns) {
		uni.showActionSheet({
			itemList: btns.map(t => t.text),
			success: function(res) {
				var callback = btns[res.tapIndex].callback;
				if (callback)
					callback(btns[res.tapIndex]);
			},
			fail: function(res) {
				console.log(res.errMsg);
			}
		});
	}

}
