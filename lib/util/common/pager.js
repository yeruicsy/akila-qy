//查询分页对象 - 绑定对象
export class QueryModel {
	constructor(
		page,
		pageCount,
		pageSize,
		totalCount,
		order,
		hasMore,
		isPulldown,
		loading,
		keyword,
		isError
	) {
		this.isPage = true;
		this.isPulldown = false;
		this.clear();
		this.page = 1;
		this.pageSize = 10;
		this.loading = false;
		this.firstLoad = true;
		this.keyword = "";
		this.isError = false;
		this.data = {};
	}

	/**
	 * 服务器返回分页对象 进行合并
	 * @param {*} pagerModel
	 */
	extends(pagerModel) {
		console.log(pagerModel)
		this.firstLoad = false;
		//this.page = pagerModel.page;
		this.pageCount = Math.ceil(pagerModel.page.total/pagerModel.page.limit);
		//this.pageSize = pagerModel.pageSize;
		this.totalCount = pagerModel.page.total;
		this.hasMore = this.isHasMore();
		this.loading = false;
		this.isPulldown = false;
	}

	/** 转换为服务器参数 */
	toRequestParams() {
		this.data.start = this.page;
		this.data.limit = this.pageSize;
		return this.data
	}

	/**
	 * 不获取更多
	 */
	noMore() {
		this.hasMore = false;
	}

	/**
	 * 是否有更多数据
	 */
	isHasMore() {
		return this.page < this.pageCount;
	}

	/**
	 *  画面加载更多业务
	 */
	get pagePreloader() {
		if (this.isPulldown) return false;
		if (this.loading) return true;
		if (this.isNoData) return false;
		return this.hasMore;
	}

	/**
	 * 是否允许加载更多
	 */
	get canLoadmore() {
		if (this.loading) return false;
		return this.hasMore;
	}

	/**
	 * 是否已确定没有数据
	 */
	get isNoData() {
		if (this.loading) return false;
		if (this.firstLoad) return false;
		return this.totalCount === 0;
	}

	/**
	 * 清空数据
	 */
	clear() {
		this.page = 1;
		this.pageCount = 0;
		this.totalCount = 0;
		this.hasMore = true;
		this.isError = false;
	}

	/**
	 * 下一页
	 */
	nextPage() {
		this.page += 1;
	}

	/**
	 * 需要分页请求
	 */
	needPageRequest(itemIndex) {
		if (this.page * this.pageSize > itemIndex) return 0;
		var needPage = 0;
		while (true) {
			if ((this.page + needPage) * this.pageSize > itemIndex) break;
			needPage++;
		}
		return needPage;
	}

	/**
	 * 下拉刷新
	 */
	requestLoading() {
		this.isPulldown = true;
		this.loading = true;
		this.isError = false;
	}

	/** 出现加载错误 */
	requestError() {
		this.isError = true;
		this.loading = false;
	}
	
	/** 请求完成 */
	requestComplete() {
		this.loading = false;
	}
}
