//请求发送对象 - 绑定对象
export class RequestModel {

	/**
	 * 构造
	 * @param {*} _data 
	 * @param {*} _loading 
	 * @param {*} _errorMessage 
	 * @param {*} _btn 
	 */
	constructor(_data, _loading, _errorMessage, _btn) {
		this.data = _data || undefined;
		this.loading = _loading || false;
		this.errorMessage = _errorMessage === false ? false : true;
		this.btn = _btn || undefined;
		this.isError = false;
		this.mask = false;
	}

	/** 清空 */
	requestLoading() {
		this.loading = true;
		this.isError = false;
	}

	/** 请求错误 */
	requestError() {
		this.loading = false;
		this.isError = true;
	}

	/** 请求完成 */
	requestComplete() {
		this.loading = false;
	}
}