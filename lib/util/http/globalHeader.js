//Http全局头部请求
export class GlobalHeader {
	/** 构造 */
	constructor() {
		this._headers = [];
	}

	/**添加 */
	add(key, value) {
		if (!value) {
			this.remove(key);
			return;
		}
		for (const item of this._headers) {
			if (item.key === key) {
				item.value = value;
				return;
			}
		}
		this._headers.push({
			key: key,
			value: value
		});
	}

	/** 删除 */
	remove(key) {
		for (const item of this._headers) {
			if (item.key === key) {
				var index = this._headers.indexOf(item);
				this._headers.splice(index, 1);
				return;
			}
		}
	}

	/** 合并  参数 headers优先  不覆盖已存在的键  */
	merge(headers) {
		if (!headers) return;
		for (const item of this._headers) {
			if (headers[item.key]) {
				continue;
			}
			headers[item.key] = item.value;
		}
	}

	/**
	 * 转对象
	 */
	toObject() {
		var result = {};
		for (const item of this._headers) {
			result[item.key] = item.value;
		}
		return result;
	}
}
