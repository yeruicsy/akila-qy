import {
	util
} from "./../index";
import {
	HttpClientUniApp
} from "./httpClient-uni-app"

//Http客户端
export class HttpClient {

	/** 实现类 */
	get implement() {
		return new HttpClientUniApp();
	}

	/**
	 * url 参数编译
	 */
	urlEncode(param, key, encode) {
		if (param == null) return "";
		var paramStr = "";
		var t = typeof param;
		if (t == "string" || t == "number" || t == "boolean") {
			paramStr +=
				"&" +
				key +
				"=" +
				(encode == null || encode ? encodeURIComponent(param) : param);
		} else {
			for (var i in param) {
				var k =
					key == null ?
					i :
					key + (param instanceof Array ? "[" + i + "]" : "." + i);
				paramStr += this.urlEncode(param[i], k, encode);
			}
		}
		return paramStr;
	}

	/** 发送 */
	get(options) {
		this.implement.get(options);
	}

	/** 发送 */
	post(options) {
		this.implement.post(options);
	}

	/** 发送 */
	put(options) {
		this.implement.put(options);
	}

	/** 发送 */
	delete(options) {
		this.implement.delete(options);
	}

	/** 上传 */
	upload(options) {
		this.implement.upload(options);
	}

}
