import {
	util
} from "./../index";
export class HttpClientUniApp {

	//网络请求处理
	utilHttpHandle(options) {
		options.sslVerify = "false";
		options.data = options.data || {};
		options.header = options.header || {};
		util.globalHeader.merge(options.header); //合并全局的头文件
		options.fail = (e) => {
			if (options.error)
				options.error(e);
		}
		return options;
	}

	/** 发送 */
	get(options) {
		options = this.utilHttpHandle(options);
		options.method = "GET";
		this.send(options);
	}

	/** 发送 */
	post(options) {
		options = this.utilHttpHandle(options);
		options.method = "POST";
		this.send(options);
	}

	/** 发送 */
	put(options) {
		options = this.utilHttpHandle(options);
		options.method = "PUT";
		this.send(options);
	}

	/** 发送 */
	delete(options) {
		options = this.utilHttpHandle(options);
		options.method = "DELETE";
		this.send(options);
	}

	/** 上传 */
	upload(pos) {
		pos = this.utilHttpHandle(pos);
		pos.filePath = pos.data && pos.data.filePath;
		pos.name = pos.data && pos.data.name;
		pos.formData = pos.formData || {};
		pos.formData.fileNum = pos.files.length;
		if (pos.files) {
			pos.files.forEach((file, index) => {
				file.name = `files${index+1}`;
				file.uri = file.file.path;
			})
		}
		this.uploadSend(pos);
	}

	/** 发送 */
	uploadSend(options) {
		if (options.loading) {
			util.loading.show();
		}
		if (options.btn && options.btn.stop) {
			options.btn.play();
		}
		uni.uploadFile(options);
	}

	/** 发送 */
	send(options) {
		
		uni.request(options);
	}

}
