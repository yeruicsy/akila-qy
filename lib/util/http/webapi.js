import {
	util
} from "./../index";
import {
	HttpClient
} from "./httpClient";

/**
 * options 注释
 * {
	 mask:true,  // 全屏遮罩  默认：false
	 btn: refs, //  vue组件对象  如果传递  会在 请求中  执行 play  stop 方法调用  用户可自己定义重复提交禁用方式
	 data:{}, //数据  get方式 自动转换为url拼接   post方式 使用  body +  json传递
	 errorMessage:bool, //是否显示错误消息，也就是静默请求，用户没有感知， 默认：false
	 header:{}, // 请求头 优先级高于 全局header  出现重复的key会优先使用此对象的值
	 success:fun(data), //成功回调  这里会自动 拿出响应报文里面的 data，自动对报文进行了处理
	 error:fun(response), //失败回调
	 complete:fun(response), //完成回调
 }
 */


export class WebApi {

	/** 发送get请求 */
	get(url, options, resolve) {
		options = options || {};
		options.url = url;
		this.setResolve(options, resolve);
		var httpClient = new HttpClient();
		this.httpOptionSetting(options, resolve);
		httpClient.get(options);
	}

	/** 发送get请求 */
	post(url, options, resolve) {
		options = options || {};
		options.url = url;
		this.setResolve(options, resolve);
		var httpClient = new HttpClient();
		this.httpOptionSetting(options, resolve);
		httpClient.post(options);
	}

	/** 发送put请求 */
	put(url, options, resolve) {
		options = options || {};
		options.url = url;
		this.setResolve(options, resolve);
		var httpClient = new HttpClient();
		this.httpOptionSetting(options, resolve);
		httpClient.put(options);
	}

	/** 发送delete请求 */
	delete(url, options, resolve) {
		options = options || {};
		options.url = url;
		this.setResolve(options, resolve);
		var httpClient = new HttpClient();
		this.httpOptionSetting(options, resolve);
		httpClient.delete(options);
	}

	/** 发送上传请求 */
	uploadFile(url, options, resolve) {
		options = options || {};
		options.url = url;
		options.formData = options.data;
		this.setResolve(options, resolve);
		var httpClient = new HttpClient();
		this.httpOptionSetting(options, resolve);
		httpClient.upload(options);
	}

	/** 设置回调 */
	setResolve(options, resolve) {
		if (!resolve)
			return;
		if (!options.success)
			options.success = result => {
				if (result === null || result === undefined)
					resolve(true);
				resolve(result);
			};
		if (!options.error)
			options.error = result => {
				resolve(false);
			};
	}

	//** 发送请求设置 */
	httpOptionSetting(options, resolve) {
		if (options.requestLoading)
			options.requestLoading()
		options.errorMessage = options.errorMessage === false ? false : true;
		var successCallback = options.success;
		var errorCallback = options.error;
		var completeCallback = options.complete;

		if (options.toRequestParams)
			options.data = options.toRequestParams();

		options.success = function(response) {
			if (response.statusCode === 401) {
				window.location.replace('/pages/user/login');
				return;
			}
			
			if (response.statusCode !== 200) {
				console.log(response);
				if (options.errorMessage) util.message.toast("网络异常");
				if (options && options.requestError)
					options.requestError();
				if (errorCallback) {
					errorCallback(response, resolve);
				}
				return;
			}
			
			var result = response.data;
			if (response.data.constructor === String) {
				if (response.data.indexOf('{') === 0 || response.data.indexOf('[]') === 0)
					result = JSON.parse(response.data);
			}
			if (result.status === 0 || result.status === 200 || result.code === 200 || result.success) {
				if (options && options.extends)
					options.extends(result.data)
				if (successCallback) successCallback(result.data, resolve);
			} else {
				if (options.errorMessage) util.message.toast(result.msg || result.message || '服务器异常');
				if (options && options.requestError)
					options.requestError();
				if (errorCallback) {
					errorCallback(result, resolve);
				}
			}
		};
		options.error = function(response) {
			if (response && response.statusCode === 401) {
				window.location.replace('/pages/user/login');
				return;
			}
			if (options && options.requestError)
				options.requestError();
			if (errorCallback) errorCallback("请求出现错误", resolve);
		};
		options.complete = function() {
			if (completeCallback) completeCallback(resolve);
			if (!options.isPage && options.mask) {
				util.loading.hide();
			}
			if (options.btn && options.btn.stop) {
				options.btn.stop();
			}
			if (options.requestComplete)
				options.requestComplete();
			options.success = undefined;
			options.error = undefined;
		}

		if (!options.isPage && options.mask) {
			util.loading.show();
		}
		if (options.btn && options.btn.stop) {
			options.btn.play();
		}
	}
}
