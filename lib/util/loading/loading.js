import {
	util
} from "./../index";
export class Loading {
	constructor(isloading) {}

	/** 显示加载中 */
	show(msg) {
		if (this.isloading) return;
		msg = msg || "请稍后...";
		uni.showLoading({
			title: msg,
			mask: true
		});
		this.isloading = true;
	}

	/** 关闭加载中 */
	hide() {
		uni.hideLoading();
		this.isloading = false;
	}
}
