import { util } from "./../index";

export class Message {

  /** 消息提示 */
  toast(options) {
    if (typeof options === "string") {
      uni.showToast({
        title: options,
        icon: "none",
      });
      return
    }
    uni.showToast(options);
  }

  /** 消息提示 */
  alert(msg, callBack, confirmText) {
    uni.showModal({
      title: '提示',
      content: msg,
      showCancel: false,
      confirmText: confirmText || "确定",
      success: function (res) {
        if (res.confirm) {
          if (callBack)
            callBack();
        }
      }
    });
  }

  /** 确认框 */
  confirm(msg, callBack, noCallBack, confirmText, cancelText) {
    uni.showModal({
      title: '提示',
      content: msg,
      confirmText: confirmText || "确定",
      cancelText: cancelText || "取消",
      success: function (res) {
        if (res.confirm) {
          if (callBack)
            callBack()
        } else if (res.cancel) {
          if (noCallBack)
            noCallBack()
        }
      }
    });
  }

}
