/**
 * 访问地址
 */
import {
	util
} from "./../index";

/**
 * 基础库
 */
class BaseUrl {
	constructor() {}

	/**初始化 */
	init(options) {
		if (!options) return;
		this._systemServer = options.systemServer;
	}

	/** 服务器地址 */
	get systemServer() {
		return this._systemServer;
	}

	/** 对象 转 url编码  */
	parseURL(url) {
		var a = document.createElement("a");
		a.href = url;
		var result = {
			source: url,
			protocol: a.protocol.replace(":", ""),
			host: a.hostname,
			port: a.port,
			query: a.search,
			params: (function() {
				var ret = {},
					seg = a.search.replace(/^\?/, "").split("&"),
					len = seg.length,
					i = 0,
					s;
				for (; i < len; i++) {
					if (!seg[i]) {
						continue;
					}
					s = seg[i].split("=");
					ret[s[0]] = s[1];
				}
				return ret;
			})(),
			file: decodeURI((a.pathname.match(/\/([^\/?#]+)$/i) || [, ""])[1]),
			hash: a.hash.replace("#", ""),
			path: a.pathname.replace(/^([^\/])/, "/$1"),
			relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [, ""])[1],
			segments: a.pathname.replace(/^\//, "").split("/")
		};
		return result;
	}

	/** url参数进行序列化 */
	getParam(path) {
		var result = {},
			param = /([^?=&]+)=([^&]+)/gi,
			match;
		while ((match = param.exec(path)) != null) {
			result[match[1]] = match[2];
		}
		return result;
	}
}

/** 用户相关地址 */
class SystemUrl {
	/** 构造 */
	constructor() {}

	/** 微信SDK配置 */
	get jsSdkConfig() {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/createJsAPISignature`;
	}

	/** 公众号登录 */
	get mpCodeLogin() {
		return `${util.url.base.systemServer}/user/loginQrCode`;
	}

	/** 公众号注册 */
	get mpRegister() {
		return `${util.url.base.systemServer}/user/registerQrCode`;
	}

	/** 获取短信验证码 */
	get getSMSCode() {
		return `${util.url.base.systemServer}/user/getSMSCode`;
	}

	/** 刷新Token */
	get token() {
		return `${util.url.base.systemServer}/user/refreshToken`;
	}

	/** 查询模板 */
	get queryTemplate() {
		return `${util.url.base.systemServer}/user/queryTemplate`;
	}

	/** 创建模板 */
	get createTemplate() {
		return `${util.url.base.systemServer}/user/createTemplate`;
	}
	
	/** 企业微信code获取用户信息 */
	get qyCodeLogin() {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/getEnterpriseWechatUserInfo`
	}
	
	/** 企业微信默认登录账号 */
	get loginAccount () {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/enterpriseWechatLogin`
	}

}

/** 资产管理相关地址 **/
class assetMaintenanceUrl {

	/** 公区工单列表 */
	get workAreaOrderPagerQuery() {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/getEnterpriseWechatWorkOrderList`;
	}
	/** 获取位置列表  */
	get getPositionList() {
		return `${util.url.base.systemServer}/energys/energy/position/getPositionList`;
	}

	/** 添加公区工单 */
	get addWorkAreaOrder() {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/addWorkOrder`;
	}

	/** 工单详情 */
	get getWorAreakOrderListDetail() {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/getWorkOrderListDetail`;
	}
	
	/** 获取位置不分页 */
	getPositionList() {
		return `${util.url.base.systemServer}/energys/energy/position/getPositionList`;
	}

	/** 上传 */
	get batchUpload() {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/wechatBatchUpload`;
	}

	/** 获取图片地址 */
	get getPictureUrl() {
		return `${util.url.base.systemServer}/energys/energy/workAreaOrder/wechatDownloadManyPicture`;
	}

	/** 获取项目详细 */
	get workOrderLogs() {
		return `${util.url.base.systemServer}/energys/energy/workAreaOrder/getWechatWorkOrderLogList`;
	}
	
	/** 发送评价 */
	get wechatWorkOrderEvaluate() {
		return `${util.url.base.systemServer}/energys/energy/workAreaOrder/wechatWorkOrderEvaluate`;
	}
	
	/** 上传工单 */
	get wechatUploadWorkOrderSignature() {	
		return `${util.url.base.systemServer}/energys/energy/workAreaOrder/wechatUploadWorkOrderSignature`;
	}
	
	/** 获取工单签名 */
	get getWorkOrderSignature() {
		return `${util.url.base.systemServer}/energys/energy/workAreaOrder/getWorkOrderSignature`;
	}
	
	/** 获取工单类型 */
	get getWorkOrderType () {
		return `${util.url.base.systemServer}/energys/energy/workOrderPlan/getworkOrderType`
	}
	
	/** 获取图片URL （企业微信windows版） */
	get getImageUrl () {
		return `${util.url.base.systemServer}/energys/energy/enterpriseWechat/uploadTemporaryPreview`
	}
	
}
/** 项目相关 **/
class projectUrl {

	/** 获取项目详细 */
	get getProjectInfo() {
		return `${util.url.base.systemServer}/project/queryById`;
	}

	/** 获取所有项目 */
	get getAllProjectInfo() {
		return `${util.url.base.systemServer}/user/getAllProjectInfo`;
	}

}
var url = {
	base: new BaseUrl(),
	system: new SystemUrl(),
	assetMaintenance: new assetMaintenanceUrl(),
	project: new projectUrl(),
};

export default url;
