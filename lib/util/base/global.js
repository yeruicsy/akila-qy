// Vue 启动全局对象保存
export class Global {
  constructor() {
    this.ui = "";
  }

  /**
   * 初始化UI标识
   */
  initUi(ui) {
    this.ui = ui;
  }

  /**
   * 初始化应用程序编码
   */
  initApplicationCode(code) {
    this.applicationCode = code;
  }

  /**
  * 初始化应用程序生命周期编号
  */
  initRuntimeId(runtimeId) {
    this.runtimeId = runtimeId;
  }
  /**
   * uniapp框架
   */
  get uniapp() {
    return "uni-app";
  }

  /**
   * 当前是否使用uniapp框架
   */
  isUniApp() {
    return this.ui === this.uniapp;
  }
}
