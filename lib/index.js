import { util } from "./util/index";
import stores from './store/store_init'

//Util插件安装入口
const install = (Vue, options) => {
  Vue.prototype.$util = util;
  
  // 设置应用程序唯一标识
  util.global.initApplicationCode(options.applicationCode);
  if (!options) return;

  //使用业务接口地址
  if (options.serverConfig) util.url.base.init(options.serverConfig);
  
  //使用UI配置
  if (options.ui) {
    util.global.initUi(options.ui);
  }
};

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(install);
}

export default {
  install,
  util,
  stores
};
